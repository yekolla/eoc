SET SERVEROUTPUT ON

declare

poid VARCHAR2(32 BYTE);

labelid VARCHAR2(10 BYTE);
descriptionid VARCHAR2(10 BYTE);
newname NVARCHAR2(1024 CHAR);

begin

poid := 'PO_POS_OO_ENTEL_8480PLUS';

newname := 'Entel Chip 84.90 Plus';

UPDATE CWTRANSLATIONS  SET TRANSLATION = newname where id in (select LABEL from CWPC_ITEM WHERE ITEMCODE= poid);

UPDATE CWTRANSLATIONS  SET TRANSLATION = newname where id in (select LABEL from CWPC_ITEMLABEL_V WHERE ITEMCODE= poid and category = 'label');

update CWPC_ITEM_VERSION_MAP set NAME = newname  where ITEMCODE= poid AND VERSIONIDENTIFIER = '1.0';

end;
/
commit;
