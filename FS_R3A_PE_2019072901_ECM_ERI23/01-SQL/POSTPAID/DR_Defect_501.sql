UPDATE cwpc_item SET baseitemcode = 'PS_PE_GEN_POS_OPT_SVC_ADDON_CBIO' WHERE itemcode = 'PS_POS_OPT_CHG_OWNERSHIP_BILL';

COMMIT;


update cwpc_itemattribute_v set defaultvalue ='Change Ownership', isproperty='0' where itemcode in('PO_POS_OPT_CHG_OWNERSHIP','PO_POS_OPT_CHG_OWNERSHIP_BILL') 
and itemattributecode ='specificationSubSubtype';
commit;